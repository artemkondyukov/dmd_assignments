CREATE OR REPLACE FUNCTION fizzbuzz()
	RETURNS void AS $$
DECLARE
	res TEXT;
BEGIN
	res := '';
	FOR i IN 1..100 LOOP
		IF i % 3 = 0 THEN
			res = res || 'Fizz';
		END IF;
		IF i % 5 = 0 THEN
			res = res || 'Buzz';
		END IF;
		IF i % 3 <> 0 AND i % 5 <> 0 THEN
			res = res || i;
		END IF;
		RAISE NOTICE '%', res;
		res := '';
	END LOOP;
END;
$$ LANGUAGE plpgsql;

