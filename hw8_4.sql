CREATE OR REPLACE FUNCTION r_add(IN r1 TEXT, IN r2 TEXT)
	RETURNS TEXT AS $$
DECLARE
	r TEXT;
	n1 INTEGER;
	n2 INTEGER;
BEGIN
	n1 := roman_to_int(r1);
	n2 := roman_to_int(r2);
	n1 := n1 + n2;
	r := int_to_roman(n1);
	RETURN r;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION r_sub(IN r1 TEXT, IN r2 TEXT)
	RETURNS TEXT AS $$
DECLARE
	r TEXT;
	n1 INTEGER;
	n2 INTEGER;
BEGIN
	n1 := roman_to_int(r1);
	n2 := roman_to_int(r2);
	n1 := n1 - n2;
	r := int_to_roman(n1);
	RETURN r;
END
$$ LANGUAGE plpgsql;
