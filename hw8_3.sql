CREATE OR REPLACE FUNCTION int_to_roman(IN i INTEGER)
	RETURNS TEXT AS $$
DECLARE
	divider INTEGER;
	result TEXT;
	tmp INTEGER;
	symbols VARCHAR(30);
	curr_sym INTEGER;
BEGIN
	IF i <= 0 THEN
		RAISE EXCEPTION 'Romans did not know negative numbers and zero. :P';
	END IF;
	result := '';
	symbols := 'mdclxvMDCLXVI';
	curr_sym := 1;
	divider := 1000000;
	WHILE divider >= 1 LOOP
		tmp := i / divider;
		IF tmp = 9 THEN
			result = result || substr(symbols, curr_sym, 1);
			result = result || substr(symbols, curr_sym-2, 1);
			i = i - 9 * divider;
		ELSIF tmp = 4 THEN
			result = result || substr(symbols, curr_sym, 1);
			result = result || substr(symbols, curr_sym-1, 1);
			i = i - 4 * divider;
		ELSE
			IF tmp >= 5 THEN
				result = result || substr(symbols, curr_sym-1, 1);
				tmp = tmp - 5;
				i = i - 5 * divider;
			END IF;
			WHILE tmp > 0 LOOP
				result = result || substr(symbols, curr_sym, 1);
				tmp = tmp - 1;
				i = i - divider;
			END LOOP;
		END IF;
		divider = divider / 10;
		curr_sym = curr_sym + 2;
	END LOOP;
	result := regexp_replace(result, '([vxlcdm])', '\1̅', 'g');
	result := upper(result);
	RETURN result;
END
$$ LANGUAGE plpgsql;
