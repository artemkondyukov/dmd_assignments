#include "table.h"

int main(int argc, char ** argv) {
	if (argc < 2) {
		std::cout << "You need to provide file name." << std::endl;
		return 0;
	}

	Table * studentsTable = new Table(argv[1]);
	std::vector<Tuple *> * tuples = studentsTable->fetchAll();

	for (int i = 0; i < tuples->size(); i++) {
		tuples->at(i)->print();
	}

	std::cout << "All gonna be just fine" << std::endl;
	return 0;
}
