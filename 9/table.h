#ifndef TABLE_H
#define TABLE_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <stdio.h>
#include <vector>
#include <unistd.h>
#include <map>
#include <functional>
#include <iomanip>

#include "tuple.h"

class Table {
private:
	std::fstream * dbFile;
	dbField * fields;
	int nAttrs;							// Total number of attributes in table
	int nVarLengthAttrs;				// Number of attributes with variable length
	int fieldsSumLength;				// Length of fixed part of one tuple (including pointers to variable part)

	std::streampos roomInfoPosition;

	std::vector<int> *roomStarts;		// Free space in file 
	std::vector<int> *roomLengths;

	int primaryKeyAttr;					// By now it's possible to have only one field as PK

	int entryCount;


public:
	Table(char *);
	~Table();
//	Table * getInstance(char *);
	std::vector<Tuple *> *fetchAll();
	std::vector<Tuple *> *search(std::vector<std::string>, std::vector<std::string>);
	void insertTuple(Tuple * tuple);
	
private:
//	void initialize();
	size_t hashString(std::string);
	Tuple* fetchOne();					// Fetches one tuple starting from current cursor
};

#endif
