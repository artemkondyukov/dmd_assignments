#include "table.h"

#define HEADER_SIGN "header"
#define HEADER_SIGN_SIZE 7
#define HEADER_ENTRY_SIZE 10
#define DB_OFFSET_LENGTH 24
#define POINTER_LENGTH 7
#define PILE_SIZE 1000

Table::Table(char * filename) {
	dbFile = new std::fstream();
	dbFile->open(filename, std::ios::in | std::ios::out/* | std::ios::app*/ | std::ios::ate);
	if (!dbFile->is_open()) {
//		std::cout << "Unable to write to file. Maybe permission problems?" << std::endl;
		throw "unable to wirte to file. Maybe permission problems?";
	}
/*
	const char * toWrite = new char[294];
	toWrite = std::string("--- --- ---\n--- --- ---\nbcd edr asd  qweertasddgfknknl000005100000250000085anot text Third TexezgdgcsrvsV00000250000009-000001QWEPOIqwepoiQWEPOIqweqweiZXCASDQWEcollisionTtext which has colli00000000000010-000001asdASDasdA\nPK\"id\" 4\n\"name\" -20\n\"email\" -50\n\"address\" -100\n        10\n        0\n        1\n        2\n        3\n        4\n        5\n        6\n        7\n        8\n        9\n  3   -70\nheader\n").c_str();
	dbFile->write(toWrite, 294);
	dbFile->close();
	return 0;
*/
	// Obtaining header
	std::string lastLine;
	dbFile->seekg(-HEADER_SIGN_SIZE, std::ios::end);
	getline(*dbFile, lastLine);
//	std::cout << lastLine << std::endl;
	assert(lastLine == HEADER_SIGN);


	//------------------------------ reading fields info --------------------------//
	dbFile->seekg(- (HEADER_SIGN_SIZE + HEADER_ENTRY_SIZE), std::ios::end);
	getline(*dbFile, lastLine);
	std::stringstream fieldInfoSS;
	fieldInfoSS << lastLine;
	fieldInfoSS >> nAttrs;

	int fieldInfoPosition = 0;
	fieldInfoSS >> fieldInfoPosition;				// Relative position (current pos is considered as 0)
//	std::cout << "fieldInfoPosition " << fieldInfoPosition << std::endl;
	dbFile->seekg(fieldInfoPosition - HEADER_ENTRY_SIZE, std::ios::cur);
	
	assert(nAttrs > 0);
	fields = new dbField[nAttrs];
	nVarLengthAttrs = 0;
	for(int i = 0; i < nAttrs; i++) {
		std::string fieldName;
		int fieldSize = 0;

		*dbFile >> fieldName;
		*dbFile >> fieldSize;

//		std::cout << fieldName << std::endl;

		if (fieldName.substr(0, 2) == std::string("PK")) {
			primaryKeyAttr = i;
			fieldName = fieldName.substr(2);
		}

		assert(fieldName.at(0) == '\"' && fieldName.at(fieldName.length()-1) == '\"');
		fieldName = fieldName.substr(1, fieldName.length()-2);

/*
		// getting string length
		int counter = 0;
		while(fieldName[counter++] != '\0');
*/

		fields[i].fieldName = new char[fieldName.length()];
		std::copy(fieldName.begin(), fieldName.end(), fields[i].fieldName);
		fields[i].fieldSize = fieldSize;
		if (fieldSize < 0) {
			nVarLengthAttrs++;
			fieldsSumLength += 2 * POINTER_LENGTH;
		}
		else {
			fieldsSumLength += fieldSize;
		}
	}
	fieldsSumLength += POINTER_LENGTH;		// pointer to next in this bucket

/*------------------DEPRECATED--------------------
	// Getting number of entries in file
	int nAttrs = 0;
	*dbFile >> nAttrs;
	std::vector<Tuple *> tuples;
	std::cout << "entry count: " << nAttrs << std::endl;
	std::cout << "fields sum length: " << fieldsSumLength << std::endl;
	
	int * starts = new int[nAttrs];
	int * lengths = new int[nAttrs];
	const char ** pKeys = new const char*[nAttrs];

	for (int i = 0; i < nAttrs; i++) {
		*dbFile >> starts[i];
	}
*/

//	*dbFile >> nAttrs;
	*dbFile >> entryCount;
	
//------------------------------ reading info about room in heap --------------//
//	getline(*dbFile, lastLine);
	getline(*dbFile, lastLine);
	std::cout << "this line must be \"$hfrn$...\": " << lastLine << std::endl;
	assert(lastLine.substr(0, 6) == "$hfrn$");
	std::stringstream pileRoomSS;
	pileRoomSS << lastLine.substr(6, lastLine.length());
	int roomsCount = 0;
	pileRoomSS >> roomsCount;
	roomStarts = new std::vector<int>(); 
	roomLengths = new std::vector<int>();
	roomInfoPosition = dbFile->tellg();
	
	for(int i = 0; i < roomsCount; i++) {
		getline(*dbFile, lastLine);
		int roomStart = 0;
		int roomLength = 0;
		std::stringstream roomInfoSS;
		roomInfoSS << lastLine;
		roomInfoSS >> roomStart;
		roomInfoSS >> roomLength;

		roomStarts->push_back(roomStart);
		roomLengths->push_back(roomLength);

//		std::cout << "room info: " << roomStarts->at(i) << " " << roomLengths->at(i) << std::endl;
	}

// ------------------INSERT TUPLE-------------------------
//	dbFile->seekp(DB_OFFSET_LENGTH, std::ios::beg);

/*
	char ** valuesToInsert = new char * [4];
	valuesToInsert[0] = const_cast<char *>("1109");
	valuesToInsert[1] = const_cast<char *>("FifthIvan");
	valuesToInsert[2] = const_cast<char *>("ivan5@gmail.com");
	valuesToInsert[3] = const_cast<char *>("5f1ress of Ivan");
	Tuple * toInsert = new Tuple(4, fields, valuesToInsert, -1, POINTER_LENGTH);
	insertTuple(toInsert, dbFile, nAttrs, fieldsSumLength, nVarLengthAttrs, nAttrs, fields, 
		roomStarts, roomLengths);
	std::cout << "attempt to insert" << std::endl;
*/

//-----------------------UPDATE TUPLE
	
	
	dbFile->seekg(roomInfoPosition, std::ios::beg);
	for (int i = 0; i < roomStarts->size(); i++) {
//		std::cout << roomStarts->at(i) << std::endl;
		std::stringstream ss;
		if (roomLengths->at(i) <= 0) continue;
//		std::cout << roomLengths->at(i) << std::endl;
		ss << std::setw(POINTER_LENGTH) << std::setfill('0') << roomStarts->at(i);
		ss << " ";
		ss << std::setw(POINTER_LENGTH) << std::setfill('0') << roomLengths->at(i);
		dbFile->write(ss.str().c_str(), ss.str().length());
	}

//----------------------PRINT TUPLES
/*
	for (int i = 0; i < tuples.size(); i++) {
		tuples.at(i)->print();
	}
*/

}

Table::~Table() {
	dbFile->close();
}

Tuple * Table::fetchOne() {
	char ** values = new char* [nAttrs];
	char * types = new char[nAttrs];

	int * varLengthPointers = new int[nVarLengthAttrs];
	int * varLengths = new int[nVarLengthAttrs];
	int * attrOrder = new int[nVarLengthAttrs];
	int varLengthCounter = 0;

	for (int j = 0; j < nAttrs; j++) {
		if (fields[j].fieldSize < 0) {
			char * tmp = new char[POINTER_LENGTH];
			dbFile->read(tmp, POINTER_LENGTH);
			varLengthPointers[varLengthCounter] = atoi(tmp);
			
			dbFile->read(tmp, POINTER_LENGTH);
			varLengths[varLengthCounter] = atoi(tmp);			
			attrOrder[varLengthCounter++] = j;
			
			continue;
		}
		values[j] = new char[fields[j].fieldSize + 1];
		dbFile->read(values[j], fields[j].fieldSize);
		values[j][fields[j].fieldSize] = 0;					// put teminator
//		std::cout << values[j] << " " << j << std::endl;
		if (std::string(fields[j].fieldSize, '$').compare(values[j]) == 0) return 0;
	}
	char * nextPtrStr = new char[POINTER_LENGTH];
	dbFile->read(nextPtrStr, POINTER_LENGTH);

	for (int j = 0; j < nVarLengthAttrs; j++) {
//		std::cout << "var length attrs. j: " << j << std::endl;
		values[attrOrder[j]] = new char[varLengths[j]];
		dbFile->seekg(varLengthPointers[j] + DB_OFFSET_LENGTH, std::ios::beg);
//		std::cout << varLengthPointers[j] << " " << varLengths[j] << std::endl;
		dbFile->read(values[attrOrder[j]], varLengths[j]);
		values[attrOrder[j]][varLengths[j]] = 0;					// put teminator
	}

	int nextPtr = atoi(nextPtrStr);

	Tuple *result = new Tuple(nAttrs, fields, values, nextPtr, POINTER_LENGTH);
/*
	std::cout << "TUPLE: " << std::endl;
	usleep(500000);
	result->print();
	std::cout << nextPtr << std::endl;
	usleep(500000);
	std::cout << "TUPLE;" << std::endl;
*/
	return result;

}

std::vector<Tuple *> * Table::fetchAll() {
	std::vector <Tuple *> *tuples = new std::vector<Tuple *> ();
	for (int i = 0; i < entryCount; i++) {
		dbFile->seekg(PILE_SIZE + DB_OFFSET_LENGTH + i * fieldsSumLength, std::ios::beg);
//		std::cout << dbFile->tellg() << std::endl;
		Tuple * nextTuple = fetchOne();
		if (nextTuple) {
			tuples->push_back(nextTuple);

	// If current tuple points to another, fetch it too
			while (tuples->back()->getNextPtr() != -1) {
//				std::cout << tuples->back()->getNextPtr() << std::endl;
				dbFile->seekg(DB_OFFSET_LENGTH + tuples->back()->getNextPtr(), std::ios::beg);
				tuples->push_back(fetchOne());
			}
		}
	}
	return tuples;
}

void Table::insertTuple(Tuple * tuple) {
	std::string sumString;
	for (int i = 0; i < tuple->getNAttrs(); i++) {
		sumString += tuple->getValues()[i];
	}
	size_t h = hashString(sumString);
	h %= nAttrs;

	std::cout << "h: " << h << std::endl;

	dbFile->seekg(PILE_SIZE + DB_OFFSET_LENGTH + h * fieldsSumLength);
	std::streampos prevTuplePos = dbFile->tellg();
	Tuple * prevTuple = fetchOne();

	std::cout << "field sum length: " << fieldsSumLength << std::endl;

	int goodRoomFix = 0;
	std::string fixLength = tuple->fixedLengthStr();
	std::cout << fixLength << std::endl;
	if (prevTuple) {
		std::cout << "PREV TUPLE IS HERE" << std::endl;
		prevTuple->print();
		int nextPtr = prevTuple->getNextPtr();
		while(nextPtr + 1) {
			std::cout << "PREV TUPLES HAS NEXT POINTER" << std::endl;
			dbFile->seekg(DB_OFFSET_LENGTH + nextPtr, std::ios::beg);
			prevTuplePos = dbFile->tellg();
			delete prevTuple;
			prevTuple = fetchOne();
		}
		
		std::cout << "Finding room in pile" << std::endl;
		std::cout << "fixed length size: " << tuple->fixedLengthSize() << std::endl;
		while(goodRoomFix < roomStarts->size() && roomLengths->at(goodRoomFix) < tuple->fixedLengthSize()) goodRoomFix++;
		std::cout << "good room fix: " << goodRoomFix << std::endl;
		std::cout << "grf room b: " << roomLengths->at(goodRoomFix) << std::endl;
		roomStarts->at(goodRoomFix) += tuple->fixedLengthSize();
		roomLengths->at(goodRoomFix) -= tuple->fixedLengthSize();
		std::cout << "grf room a: " << roomStarts->at(goodRoomFix) << " " << roomLengths->at(goodRoomFix) << std::endl;
		prevTuplePos += fieldsSumLength - POINTER_LENGTH;
		dbFile->seekg(prevTuplePos, std::ios::beg);
		goodRoomFix = DB_OFFSET_LENGTH + roomStarts->at(goodRoomFix) - tuple->fixedLengthSize();
		std::stringstream pointerSS;
		pointerSS << std::setw(POINTER_LENGTH) << std::setfill('0') << goodRoomFix - DB_OFFSET_LENGTH;
		dbFile->write(pointerSS.str().c_str(), pointerSS.str().length());
	}
	else {
		goodRoomFix = PILE_SIZE + DB_OFFSET_LENGTH + h * fieldsSumLength;
		std::cout << "good room fix: " << goodRoomFix << std::endl;
	}
	
	std::vector<std::string> varLength = tuple->varLengthValues();
	std::stringstream ss;
	for (int i = 0; i < varLength.size(); i++) {
		int goodRoomVar = 0;
		while(goodRoomVar < roomStarts->size() && roomLengths->at(goodRoomVar) < varLength.at(i).length()) goodRoomVar++;
//		std::cout << "good room var " << i << ": " << roomStarts->at(goodRoomVar) << std::endl;
//		std::cout << "grv room: " << roomLengths << std::endl;
		dbFile->seekg(DB_OFFSET_LENGTH + roomStarts->at(goodRoomVar), std::ios::beg);
		dbFile->write(varLength.at(i).c_str(), varLength.at(i).length());
		ss << std::setw(POINTER_LENGTH) << std::setfill('0') << roomStarts->at(goodRoomVar);
		ss << std::setw(POINTER_LENGTH) << std::setfill('0') << varLength.at(i).length();
		roomStarts->at(goodRoomVar) += varLength.at(i).length();
		this->roomLengths->at(goodRoomVar) -= varLength.at(i).length();
//		string s = ss.str();
//		fixLength += ss.str();
	}
	ss << '-' << std::setw(POINTER_LENGTH - 1) << std::setfill('0') << 1;
	fixLength += ss.str();

	std::cout << fixLength << std::endl;
	dbFile->seekg(goodRoomFix, std::ios::beg);
	dbFile->write(fixLength.c_str(), fixLength.length());
	
}

std::vector<Tuple *> * Table::search(std::vector<std::string> names, std::vector<std::string> values) {
	return 0;
}

size_t Table::hashString(std::string in) {
	std::hash<std::string> h_fn;
	size_t str_hash = h_fn(in);
	return str_hash;
}

