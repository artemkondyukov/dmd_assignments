#ifndef TUPLE_H
#define TUPLE_H

#include <iostream>
#include <string>
#include <vector>

struct dbField {
	char * fieldName;
	int fieldSize;
	char type;

	dbField() { type = 's'; }
};

class Tuple {
private:
	int nAttrs;
	dbField * fields;	// pointer to fileds in current tuple
	char ** values;		// we decode/encode to/from char representaion (e.g. int is broken into 4 parts)
	int nextPointer;	// relative position to the next tuple
	int pointerSize;

public:
	Tuple(int nAttrs);
	Tuple(int nAttrs, dbField * fields, char ** values, int nextPointer, int pointerSize);
	void print();

	int getNextPtr();
	void setNextPtr(int p);

	int getNAttrs();
	char ** getValues();

	int fixedLengthSize();	
	std::string fixedLengthStr();
	std::vector<std::string> varLengthValues();
};

#endif
