#include "tuple.h"

Tuple::Tuple(int nAttrs) {
	this->nAttrs = nAttrs;
	fields = new dbField[nAttrs];
	values = new char *[nAttrs];
}

Tuple::Tuple(int nAttrs, dbField * fields, char ** values, int nextPointer, int pointerSize) {
	this->nAttrs = nAttrs;
	this->fields = fields;
	this->values = values;
	this->nextPointer = nextPointer;
	this->pointerSize = pointerSize;
}

void Tuple::print() {
	std::cout << "attributes count: " << nAttrs << std::endl;
	for (int i = 0; i < nAttrs; i++ ){
		std::cout << fields[i].fieldName << ": " << values[i] << std::endl;
	}
}

int Tuple::getNextPtr() {
	return this->nextPointer;
}

void Tuple::setNextPtr(int p) {
	this->nextPointer = p;
}

int Tuple::getNAttrs() {
	return this->nAttrs;
}

char ** Tuple::getValues() {
	return this->values;
}

int Tuple::fixedLengthSize() {
	int result = 0;
	for (int i = 0; i < nAttrs; i++) {
		if (fields[i].fieldSize > 0) result += fields[i].fieldSize;
		else result += pointerSize * 2;
	}
	result += pointerSize;
	return result;
}

std::string Tuple::fixedLengthStr() {
	std::string result;
	for (int i = 0; i < nAttrs; i++) {
		if (fields[i].fieldSize > 0) result += values[i];
	}
	return result;
}

std::vector<std::string> Tuple::varLengthValues() {
	std::vector<std::string> result;
	for (int i = 0; i < nAttrs; i++) {
		if (fields[i].fieldSize < 0) result.push_back(std::string(values[i]));
	}
	return result;
}
