CREATE OR REPLACE FUNCTION roman_to_int(IN r TEXT)
	RETURNS INTEGER AS $$
DECLARE
	last_char_mant BOOLEAN;    -- TRUE is 5, FALSE is 1
	last_char_exp INTEGER;     -- 0 is for I and V, 1 is for X and L etc.
        	                   -- e.g. {TRUE, 2} is D
	curr_char_mant BOOLEAN;
	curr_char_exp INTEGER;
	roman TEXT;
	result INTEGER;
	ones VARCHAR(10);
	fives VARCHAR(10);
	fives_arr CHARACTER ARRAY[6];
	length INTEGER;
	same_in_row INTEGER;         -- to avoid XXXXX, CCCC etc.
	almost_restricted INTEGER;   -- to avoid VIX, also, occurrence of X restrict further presence of C if they are not adjacent
	restricted INTEGER;
BEGIN
	roman := r;
	same_in_row := 1;
	result := 0;
	last_char_mant := FALSE;
	last_char_exp := -1;
	curr_char_mant := FALSE;
	curr_char_exp := -1;
	IF array_length(regexp_matches(roman, '([a-z])'), 1) THEN
		RAISE EXCEPTION 'You are not allowed to use lowercase!';
	END IF;
	roman := regexp_replace(roman, '(V̅|̲V|V̅|V̲)', 'v', 'g');
	roman := regexp_replace(roman, '(X̅|̲X|X̅|X̲)', 'x', 'g');    -- convert symbols for simpler modification
	roman := regexp_replace(roman, '(̅L|̲L|L̅|L̲)', 'l', 'g');
	roman := regexp_replace(roman, '(̅C|̲C|C̅|C̲)', 'c', 'g');
	roman := regexp_replace(roman, '(̅D|̲D|D̅|D̲)', 'd', 'g');
	roman := regexp_replace(roman, '(̅M|̲M|M̅|M̲)', 'm', 'g');

	IF position('_' in roman) <> 0 THEN
		RAISE EXCEPTION 'Invalid format of roman. Redundant ''_'' ';    -- if _ was at the end, or there were two adjacent '_'
	END IF;

	ones := 'IXCMxcm';    -- for simpler conversion to curr format
	fives := 'VLDvld';
	fives_arr := regexp_split_to_array(fives, '');

	FOR i IN 1..array_upper(fives_arr, 1) LOOP
		IF length(roman) - length(replace(roman, fives_arr[i], '')) > 1 THEN
			RAISE EXCEPTION 'Invalid format of roman (DD or VIV)';
		END IF;
	END LOOP;

	length := char_length(roman);
	FOR i IN 1..length LOOP
		IF position(substr(roman, i, 1) in ones) <> 0 THEN
			curr_char_mant := FALSE;
			curr_char_exp := position(substr(roman, i, 1) in ones) - 1;
		ELSIF position(substr(roman, i, 1) in fives) <> 0 THEN
			curr_char_mant := TRUE;
			curr_char_exp := position(substr(roman, i, 1) in fives) - 1;
		ELSE
			RAISE EXCEPTION 'Invalid format of roman (some sybmols are wrong) %', roman;
		END IF;

		IF last_char_exp = -1 THEN
			result = (1 + 4 * curr_char_mant::INTEGER) * 10 ^ curr_char_exp;
			almost_restricted := (1 + 4 * curr_char_mant::INTEGER) * 10 ^ curr_char_exp;
		ELSE
			IF (1 + 4 * curr_char_mant::INTEGER) * 10 ^ curr_char_exp > restricted THEN
				RAISE EXCEPTION 'Invalid roman format (VIX)';   
			END IF;
			IF last_char_exp - curr_char_exp < -1 OR (last_char_exp < curr_char_exp AND last_char_mant < curr_char_mant) THEN
				RAISE EXCEPTION 'Invalid roman format (IC or VM or IL)';    -- situations like IC or VM
			ELSIF last_char_exp - curr_char_exp = -1 AND last_char_mant THEN
				RAISE EXCEPTION 'Invalid roman format (VL)';    -- situations like VL (correct is XLV)
			ELSIF last_char_exp > curr_char_exp OR (last_char_exp = curr_char_exp AND last_char_mant >= curr_char_mant) THEN
				restricted := almost_restricted;
				result := result + (1 + 4 * curr_char_mant::INTEGER) * 10 ^ curr_char_exp;
				almost_restricted := (1 + 4 * curr_char_mant::INTEGER) * 10 ^ curr_char_exp;
			ELSIF last_char_exp < curr_char_exp OR (last_char_exp = curr_char_exp AND last_char_mant < curr_char_mant) THEN
				IF same_in_row > 1 THEN
					RAISE EXCEPTION 'Invalid roman format (XXL or CCCM)';    -- situations like XXL (correct is XXX)
				ELSE
					result := result - 2 * (1 + 4 * last_char_mant::INTEGER) * 10 ^ last_char_exp;    -- 2 here because XIV is 10 + 1 - (2 * 1) + 5
					result := result + (1 + 4 * curr_char_mant::INTEGER) * 10 ^ curr_char_exp;
					restricted := almost_restricted / 2;
				END IF;
			END IF;
		END IF;

		IF last_char_mant = curr_char_mant AND last_char_exp = curr_char_exp THEN
			same_in_row = same_in_row + 1;
		ELSE
			last_char_mant := curr_char_mant;
			last_char_exp := curr_char_exp;
			same_in_row = 1;
		END IF;

		IF same_in_row = 4 THEN
		RAISE EXCEPTION 'Invalid format of roman (XXXX)';
		END IF;
	END LOOP;
	RETURN result;
END;
$$ LANGUAGE plpgsql;
